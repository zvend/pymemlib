from enum import Enum


class CallConvention(Enum):
    STDCALL  = 1
    FASTCALL = 2
    THISCALL = 3

    def __get__(self, instance, owner):
        return self.value


class Hook(object):
    """
    Callable object that can contains information about the calling convention
    and the types of his params. Useful to hook function compiled in C.
    e.g.

    @Hook.stdcall(DWORD)
    def OnSleep(dwMilliseconds):
        print(f'Sleep for {dwMilliseconds} ms')
    OnSleep(25) # prints 'Sleep for 25 ms'

    Note: The function can also be a method.


    class WatchProcess(object):
        def __init__(self, name):
            self.name = name
        @Hook.stdcall(DWORD)
        def OnSleep(self, dwMilliseconds):
            print(f'Process {self.name} Sleep for {dwMilliseconds} ms')

    p1 = WatchProcess('Notepad')
    s1 = p1.OnSleep

    s1(10) # prints 'Process Notepad Sleep for 10 ms'

    p2 = WatchProcess('Vim')
    s2 = p2.OnSleep

    s2(20) # prints 'Process Vim Sleep for 20 ms'

    """

    def __init__(self, callback, callconv, argtypes):
        self.callback   = callback
        self.callconv   = callconv
        self.argtypes   = argtypes

        self.argstr     = ''.join(arg._type_ for arg in argtypes)
        self.extargs    = []

    def __hash__(self):
        return hash(self.callback)

    def __repr__(self):
        return repr(self.callback)

    def __str__(self):
        return str(self.callback)

    def __get__(self, inst, parent):
        hook = self.clone()
        hook.extargs = [inst]
        return hook

    def __call__(self, *args, **kw):
        return self.callback(*self.extargs, *args, **kw)

    def clone(self):
        return Hook(self.callback, self.callconv, self.argtypes)

    def stdcall(*argtypes):
        def wrapper(function):
            process = Hook(function, CallConvention.STDCALL, argtypes)
            return process

        return wrapper

    def fastcall(*argtypes):
        def wrapper(function):
            process = Hook(function, CallConvention.FASTCALL, argtypes)
            return process

        return wrapper

    def thiscall(*argtypes):
        def wrapper(function):
            process = Hook(function, CallConvention.THISCALL, argtypes)
            return process

        return wrapper