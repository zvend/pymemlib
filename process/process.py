import struct
from _ctypes import byref, sizeof

from kernel_lib.defines import DWORD, PYTHON_IS_64_BITS, BOOL
from kernel_lib.functions import (
    create_buffer,
    CreateToolhelp32Snapshot,
    Process32Next,
    CloseHandle,
    OpenProcess,
    TerminateProcess,
    WaitForSingleObject,
    GetExitCodeProcess,
    FlushInstructionCache,
    WriteProcessMemory,
    ReadProcessMemory,
    VirtualAllocEx,
    VirtualFreeEx,
    Thread32Next,
    Module32Next,
    Module32First,
    VirtualQueryEx,
    CreateRemoteThread,
    IsWow64Process
)
from kernel_lib.includes.synchapi import INFINITE, WAIT_OBJECT_0
from kernel_lib.includes.tlhelp32 import (
    THREADENTRY32,
    MODULEENTRY32,
    ToolhelpFlags as THFlag,
    PROCESSENTRY32
)
from kernel_lib.includes.winnt import (
    ProcessAccessRight as AccessRight,
    MemoryProtection as Protection,
    MemoryAllocationType as AllocationType,
    MEMORY_BASIC_INFORMATION,
    MEMORY_BASIC_INFORMATION64
)
from kernel_lib.process.constants import MemoryFormat
from kernel_lib.process.process_module import ProcessModule
from kernel_lib.process.process_thread import ProcessThread
from kernel_lib.windows.error import SystemErrorCode
from kernel_lib.windows.exception import Win32Exception


def GetProcesses(access_right = AccessRight.PROCESS_ALL_ACCESS):
    entries = list()
    buffer = PROCESSENTRY32()
    snapshot = CreateToolhelp32Snapshot(THFlag.TH32CS_SNAPPROCESS, 0)

    if not snapshot:
        raise Win32Exception()

    while Process32Next(snapshot, byref(buffer)):
        try:
            process = Process(buffer.th32ProcessID, access_right)
            entries.append(process)
        except Win32Exception:
            pass

    CloseHandle(snapshot)

    return entries


def GetProcessesByName(name, access_right = AccessRight.PROCESS_ALL_ACCESS):
    """Returns a list of Process that have the give name."""

    name        = name.encode('ascii').lower()
    entries     = list()
    buffer      = PROCESSENTRY32()
    snapshot    = CreateToolhelp32Snapshot(THFlag.TH32CS_SNAPPROCESS, 0)

    if not snapshot:
        raise Win32Exception()

    while Process32Next(snapshot, byref(buffer)):

        if buffer.szExeFile.lower() == name:
            entries.append(Process(buffer.th32ProcessID, access_right))

    CloseHandle(snapshot)

    return entries


class Process(object):
    """
    An object to interact with a Win32 remote process.
    Properties:
        - id
        - handle
        - name
        - threads
        - modules
    """

    def __init__(self, pid, access_right = AccessRight.PROCESS_ALL_ACCESS):
        self.id     = pid
        self.handle = OpenProcess(access_right, False, pid)

        if not self.handle:
            raise Win32Exception()

    def __del__(self):
        CloseHandle(self.handle)

    def __eq__(self, other):
        return self.id == other.id

    def __repr__(self):
        return f'<Process {self.id}, handle {self.handle} at 0x{id(self):08X}>'

    @property
    def name(self):
        """Returns the name of the process."""

        return self.module().name

    def kill(self, code = SystemErrorCode.ERROR_SUCCESS):
        """Terminates the Process with the given exit code."""

        success = TerminateProcess(self.handle, code)

        if not success:
            raise Win32Exception()

    def join(self, timeout = INFINITE):
        """Waits until the Process terminates and Returns exit code"""

        reason = WaitForSingleObject(self.handle, timeout)

        if reason != WAIT_OBJECT_0:
            raise RuntimeError('Thread has been terminated prematurely.')

        code    = DWORD()
        success = GetExitCodeProcess(self.handle, byref(code))

        if not success:
            raise Win32Exception()

        return code.value

    def flush(self, address = None, size=0):
        """Flush the instruction cache for the process"""

        FlushInstructionCache(self.handle, address, size)

    def write(self, address, *data):
        """Writes in remote process at the given address the data."""

        binary  = b''.join(data)
        success = WriteProcessMemory(self.handle, address, binary, len(binary), None)

        if not success:
            raise Win32Exception()

    def read(self, address, format = MemoryFormat.DWORD):
        """Reads in remote process at given address and return the given format."""

        size    = struct.calcsize(format)
        buffer  = create_buffer(size)
        success = ReadProcessMemory(self.handle, address, byref(buffer), size, None)

        if not success:
            raise Win32Exception()

        data = struct.unpack(format, buffer)
        if len(data) == 1:
            return data[0]

        return struct.unpack(format, buffer)

    def mmap(self, size, address = None):
        """Allocates memory from the unmanaged memory of the process by using the specified number of bytes."""

        protect     = Protection.PAGE_EXECUTE_READWRITE
        alloc_type  = AllocationType.MEM_COMMIT | AllocationType.MEM_RESERVE
        memory      = VirtualAllocEx(self.handle, address, size, alloc_type, protect)

        if not memory:
            raise Win32Exception()

        return memory

    def unmap(self, address):
        """Releases memory previously allocated from the unmanaged memory of the process."""
        VirtualFreeEx(self.handle, address, 0, AllocationType.MEM_RELEASE)

    @property
    def threads(self):
        """Returns the ProcessThread's list of the process."""

        buffer  = THREADENTRY32()
        snap    = CreateToolhelp32Snapshot(THFlag.TH32CS_SNAPTHREAD, self.id)

        if not snap:
            raise Win32Exception()

        threads = list()

        while Thread32Next(snap, byref(buffer)):

            if buffer.th32OwnerProcessID == self.id:
                threads.append(ProcessThread(buffer.th32ThreadID, self))

        CloseHandle(snap)

        return threads

    @property
    def modules(self):
        """Returns the ProcessModule's list of the process."""

        buffer      = MODULEENTRY32()
        snapshot    = CreateToolhelp32Snapshot(THFlag.TH32CS_SNAPMODULE | THFlag.TH32CS_SNAPMODULE32, self.id)

        if not snapshot:
            raise Win32Exception()

        modules = list()

        while Module32Next(snapshot, byref(buffer)):
            modules.append(ProcessModule.from_MODULEENTRY32(buffer, self))

        CloseHandle(snapshot)

        return modules

    def module(self, name=None):
        """Return a ProcessModule's instance of a given module's name."""

        buffer      = MODULEENTRY32()
        snapshot    = CreateToolhelp32Snapshot(THFlag.TH32CS_SNAPMODULE | THFlag.TH32CS_SNAPMODULE32, self.id)

        if not snapshot:
            raise Win32Exception()

        if name is None:
            rv = Module32First(snapshot, byref(buffer))  # can this even fail if we were able to open the snapshot ?
            CloseHandle(snapshot)

            return ProcessModule.from_MODULEENTRY32(buffer, self)

        name = name.encode('ascii')
        while Module32Next(snapshot, byref(buffer)):

            if buffer.szModule == name:
                CloseHandle(snapshot)

                return ProcessModule.from_MODULEENTRY32(buffer, self)

        raise RuntimeError(f"No process module with the name '{name}' was found.")

    def page_info(self, address):
        """Returns (size, access right) about a range of pages in the virtual address space of a process."""

        if not PYTHON_IS_64_BITS:
            buffer = MEMORY_BASIC_INFORMATION()
        else:
            buffer = MEMORY_BASIC_INFORMATION64()

        VirtualQueryEx(self.handle, address, byref(buffer), sizeof(buffer))

        return buffer.RegionSize, buffer.Protect

    def spawn_thread(self, entry, param=None):
        """Spawns a thread in a suspended state and returns his ProcessThread."""
        id = DWORD()
        handle = CreateRemoteThread(self.handle, 0, 0, entry, param, 4, byref(id))

        if not handle:
            raise Win32Exception()

        return ProcessThread(id, self, handle)

    def is32bit(self):
        wow64 = BOOL()

        if not IsWow64Process(self.handle, byref(wow64)):
            raise Win32Exception()

        return wow64.value == 1

    @classmethod
    def from_name(cls, name):
        """Creates a Process from his name."""

        processes = GetProcessesByName(name)

        if not len(processes):
            raise RuntimeError('No process with the given name was found.')

        return processes[0]