from _ctypes import byref, sizeof

from kernel_lib.defines import DWORD
from kernel_lib.functions import (
    OpenThread,
    CloseHandle,
    TerminateThread,
    GetExitCodeThread,
    ResumeThread,
    SuspendThread,
    WaitForSingleObject,
    GetThreadContext,
    SetThreadContext,
    NtQueryInformationThread
)
from kernel_lib.includes.ntifs import THREADINFOCLASS as Information
from kernel_lib.includes.ntpsapi import THREAD_BASIC_INFORMATION
from kernel_lib.includes.processthreadsapi import STILL_ACTIVE
from kernel_lib.includes.synchapi import INFINITE, WAIT_OBJECT_0
from kernel_lib.includes.winnt import ThreadAccessRight, ContextFlag, WOW64_CONTEXT
from kernel_lib.windows.error import SystemErrorCode
from kernel_lib.windows.exception import Win32Exception


class ProcessThread(object):
    """
    A Win32 process thread object.
    Properties:
        - id
        - pid
        - handle
        - alive
        - running
    """

    def __init__(self, id, process, handle = None):
        self.id     = id
        self.pid    = process.id
        self.handle = handle or OpenThread(ThreadAccessRight.THREAD_ALL_ACCESS, False, id)

        if not self.handle:
            raise Win32Exception()

    def __del__(self):
        CloseHandle(self.handle)

    def __eq__(self, other):
        return self.id == other.id

    def __repr__(self):
        return f'<ProcessThread {self.id} in Process {self.pid}>'

    def kill(self, code = SystemErrorCode.ERROR_SUCCESS):
        """Terminates the Thread with the given exit code."""

        success = TerminateThread(self.handle, code)
        if not success:
            raise Win32Exception()

    @property
    def alive(self):
        """Checks if the thread is still executing."""

        code    = DWORD()
        success = GetExitCodeThread(self.handle, byref(code))

        if not success:
            raise Win32Exception()

        return code.value == STILL_ACTIVE

    def resume(self):
        """Resumes the thread."""

        count = ResumeThread(self.handle)

        if count == 0xFFFFFFFF:     # DWORD - 1 = 0xFFFFFFFF
            raise Win32Exception()

    def suspend(self):
        """Suspends the thread."""

        count = SuspendThread(self.handle)

        if count == 0xFFFFFFFF:     # DWORD - 1 = 0xFFFFFFFF
            raise Win32Exception()

    def join(self, timeout = INFINITE):
        """Waits until the thread exit and returns the exit code."""

        reason = WaitForSingleObject(self.handle, timeout)

        if reason != WAIT_OBJECT_0:
            raise RuntimeError('Thread has been terminated prematurely.')

        code    = DWORD()
        success = GetExitCodeThread(self.handle, byref(code))

        if not success:
            raise Win32Exception()

        return code.value

    def context(self, flags = ContextFlag.CONTEXT_FULL):
        """Retrieves the context of the ProcessThread."""

        context                 = WOW64_CONTEXT()
        context.ContextFlags    = flags

        if not GetThreadContext(self.handle, byref(context)):
            raise Win32Exception()

        return context

    def set_context(self, context):
        """Sets the context for the ProcessThread."""

        success = SetThreadContext(self.handle, byref(context))

        if not success:
            raise Win32Exception()

    @property
    def teb(self):
        """Returns the address of the thread's information/environment block"""

        info        = THREAD_BASIC_INFORMATION()
        ntstatus    = NtQueryInformationThread(
            self.handle,
            Information.ThreadBasicInformation,
            byref(info),
            sizeof(info),
            None
        )

        # Need confirmation, but WinDll default returns type is int and
        # NTSUCCESS are positive signed 32 bytes integers [0 - 0x7FFFFFFF]
        if ntstatus < 0:
            raise Win32Exception()

        return info.TebBaseAddress