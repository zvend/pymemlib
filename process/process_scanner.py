from kernel_lib.process.constants import MemoryFormat


class ProcessScanner(object):
    """
    Class used to scan remote process's code section.
    """

    def __init__(self, process):
        self.process    = process
        self.module     = process.module()

        if not self.module:
            raise RuntimeError("Couldn't find default module")

        # Very hacky but that will do it for now
        self.base       = self.module.base
        self.buffer    = process.read(self.base, f'{self.module.size}{MemoryFormat.CHAR_ARRAY}')

    def find(self, pattern, offset = 0):
        """Returns address of the pattern if found."""

        match = self.buffer.find(pattern)

        if not match:
            raise RuntimeError("Couldn't find the pattern.")

        return self.base + match + offset

    def __repr__(self):
        return f'<Scanner 0x{id(self):08X} for Process {self.process.id}>'
