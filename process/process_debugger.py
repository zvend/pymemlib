from _ctypes import byref
from contextlib import suppress

from kernel_lib.functions import DebugSetProcessKillOnExit, DebugActiveProcess, DebugActiveProcessStop, \
    WaitForDebugEvent, ContinueDebugEvent
from kernel_lib.includes.winnt import NTSTATUS as WINNT
from kernel_lib.includes.minwinbase import (
    DEBUG_EVENT,
    DebugEventCode as EventCode
)
from kernel_lib.includes.synchapi import INFINITE
from kernel_lib.process.hook import Hook
from kernel_lib.process.process_hook import ProcessHook
from kernel_lib.process.process_thread import ProcessThread
from kernel_lib.windows.error import SystemErrorCode
from kernel_lib.windows.exception import Win32Exception


class ProcessDebugger(object):
    """
    """

    def __init__(self, process = None):
        self.hooks      = dict()
        self.attached   = False
        self.process    = None
        self.exit_code  = SystemErrorCode.ERROR_SUCCESS
        DebugSetProcessKillOnExit(False)

        if process is not None:
            self.attach(process)

    def __del__(self):
        self.detach()

    def __repr__(self):
        return f'<ProcessDebugger for Process {self.process.id}>'

    def add_hook(self, address, hook):
        process_hook = ProcessHook(self.process, address, hook)
        
        if address in self.hooks:
            old_hook = self.hooks[address]
            old_hook.disable()

        self.hooks[address] = process_hook
        process_hook.enable()

    def attach(self, process):

        if self.attached:
            raise RuntimeError('ProcessDebugger already attached!')

        self.process = process

        if not DebugActiveProcess(process.id):
            raise Win32Exception()

        self.attached = True

    def detach(self):

        if not self.attached:
            return

        with suppress(Exception):

            for hook in self.hooks.values():
                hook.disable()

        DebugActiveProcessStop(self.process.id)
        self.attached = False

    def run(self, **kw):
        frequency = kw.pop('frequency', INFINITE)

        while self.attached:
            self.poll(frequency)

    def poll(self, timeout = INFINITE):
        """Poll the next event dispatch it"""

        evt = DEBUG_EVENT()

        while WaitForDebugEvent(byref(evt), timeout):

            if evt.dwProcessId != self.process.id:
                ContinueDebugEvent(evt.dwProcessId, evt.dwThreadId, WINNT.DBG_EXCEPTION_NOT_HANDLED)
                return

            continue_status = WINNT.DBG_CONTINUE
            event_code      = evt.dwDebugEventCode
            thread          = ProcessThread(evt.dwThreadId, self.process)

            if event_code == EventCode.EXCEPTION_DEBUG_EVENT:
                continue_status = self._on_debug_event(thread, evt.u.Exception)

            elif event_code == EventCode.CREATE_THREAD_DEBUG_EVENT:
                self.OnCreateThreadDebugEvent(thread, evt.u.CreateThread)

            elif event_code == EventCode.CREATE_PROCESS_DEBUG_EVENT:
                self.OnCreateProcessDebugEvent(thread, evt.u.CreateProcessInfo)

            elif event_code == EventCode.EXIT_THREAD_DEBUG_EVENT:
                self.OnExitThreadDebugEvent(thread, evt.u.ExitThread)

            elif event_code == EventCode.EXIT_PROCESS_DEBUG_EVENT:
                self.exit_code = evt.u.ExitProcess.dwExitCode
                self.OnExitProcessDebugEvent(thread, evt.u.ExitProcess)
                self.detach()

            elif event_code == EventCode.LOAD_DLL_DEBUG_EVENT:
                self.OnLoadDllDebugEvent(thread, evt.u.LoadDll)

            elif event_code == EventCode.UNLOAD_DLL_DEBUG_EVENT:
                self.OnUnloadDllDebugEvent(thread, evt.u.UnloadDll)

            elif event_code == EventCode.OUTPUT_DEBUG_STRING_EVENT:
                self.OnOutputDebugStringEvent(thread, evt.u.DebugString)

            elif event_code == EventCode.RIP_EVENT:
                self.OnRipEvent(evt.u.RipInfo)

            ContinueDebugEvent(evt.dwProcessId, evt.dwThreadId, continue_status)

    def _on_single_step(self, thread, address):
        hook = self.ss_hook

        if not hook:
            return WINNT.DBG_EXCEPTION_NOT_HANDLED

        hook.enable()

        return WINNT.DBG_CONTINUE

    def _on_breakpoint(self, thread, address):
        process_hook = self.hooks.get(address, None)

        if not process_hook:
            return WINNT.DBG_EXCEPTION_NOT_HANDLED

        process_hook.disable()

        ctx  = thread.context()
        args = list()
        hook = process_hook.hook
        conv = hook.callconv
        argc = len(hook.argtypes)

        if (argc >= 1) and ((conv == Hook.fastcall) or (conv == Hook.thiscall)):
            args.append(ctx.Ecx)

        if (argc >= 2) and (conv == Hook.fastcall):
            args.append(ctx.Edx)

        argstr      = hook.argstr[len(args):]
        argc        = argc - len(args)
        stack_args  = self.process.read(ctx.Esp + 4, argstr)
        args.extend(stack_args)

        # What should we do here ??? (We don't want to crash the remote processes)
        with suppress(Exception):
            hook(*args)

        # This will enable a "single-step" breakpoint
        # We cannot reach an other breakpoint before raising Single-Step exception
        self.ss_hook = process_hook
        ctx.Eip     -= 1
        ctx.EFlags  |= 0x100  # TRAP_FLAG
        thread.set_context(ctx)

        return WINNT.DBG_CONTINUE

    def _on_debug_event(self, thread, info):
        """This is internal, but if you were to overload it, return the continuation status"""

        code = info.ExceptionCode

        if info.dwFirstChance != 1:
            return WINNT.DBG_EXCEPTION_NOT_HANDLED

        address = info.ExceptionAddress

        if code == WINNT.EXCEPTION_SINGLE_STEP:
            return self._on_single_step(thread, address)

        if code == WINNT.EXCEPTION_BREAKPOINT:
            return self._on_breakpoint(thread, address)

        return WINNT.DBG_EXCEPTION_NOT_HANDLED

    def OnCreateThreadDebugEvent(self, thread, info):
        """Can be overloaded to hook this event"""
        pass

    def OnExitThreadDebugEvent(self, thread, info):
        """Can be overloaded to hook this event"""
        pass

    def OnCreateProcessDebugEvent(self, thread, info):
        """Can be overloaded to hook this event"""
        pass

    def OnExitProcessDebugEvent(self, thread, info):
        """Can be overloaded to hook this event"""
        pass

    def OnLoadDllDebugEvent(self, thread, info):
        """Can be overloaded to hook this event"""
        pass

    def OnUnloadDllDebugEvent(self, thread, info):
        """Can be overloaded to hook this event"""
        pass

    def OnOutputDebugStringEvent(self, thread, info):
        """Can be overloaded to hook this event"""
        pass

    def OnRipEvent(self):
        """Can be overloaded to hook this event"""
        pass