class ProcessHook(object):
    """ """

    def __init__(self, process, address, hook):
        self.process    = process
        self.address    = address
        self.hook       = hook
        self.inst       = process.read(address, 's')[0]
        self.enabled    = False

    def __del__(self):

        if self.enabled:
            self.disable()

    def __repr__(self):
        return f'<ProcessHook 0x{self.address:08X} in Process {self.process.id}>'

    def enable(self):
        self.enabled = True
        self.process.write(self.address, b'\xCC')
        self.process.flush(self.address, 1)

    def disable(self):
        self.enabled = False
        self.process.write(self.address, self.inst)
        self.process.flush(self.address, 1)