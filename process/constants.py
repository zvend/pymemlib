from enum import Enum


class MemoryFormat(Enum):
    """
    An Enum to indicate the format of the values to read from the memory.

    Refs: https://docs.python.org/3/library/struct.html#format-characters
    """

    # C Type            Format  Size(Bytes)
    PAD_BYTE            = 'x'
    CHAR                = 'c'   # 1
    SIGNED_CHAR         = 'b'   # 1
    BYTE                = 'B'   # 1
    BOOL                = '?'   # 1
    SHORT               = 'h'   # 2
    WORD                = 'H'   # 2
    INT                 = 'i'   # 4
    DWORD               = 'I'   # 4
    LONG2               = 'l'   # 4
    DWORD2              = 'L'   # 4
    LONG                = 'q'   # 8
    QWORD               = 'Q'   # 8
    SSIZE_T             = 'n'
    SIZE_T              = 'N'
    HALF_FLOAT          = 'e'   # 2
    FLOAT               = 'f'   # 4
    DOUBLE              = 'd'   # 8
    CHAR_ARRAY          = 's'
    CHAR_ARRAY_P        = 'p'
    VOID                = 'P'   #

    def __get__(self, instance, owner):
        return self.value