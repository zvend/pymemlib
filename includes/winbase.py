from kernel_lib.defines import WCHAR
from kernel_lib.functions import FormatMessageW, GetLastError
from kernel_lib.windows.error import SystemErrorCode


def FormatMessage(error):
    """
    Format a Win32 error code to the corresponding message. (See MSDN)

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-formatmessage
    """

    size = 256
    while size < 0x10000:  # 0x10000 is the constant choosed in C# standard lib.
        buffer = (WCHAR * size)()
        result = FormatMessageW(0x200 | 0x1000 | 0x2000, None, error, 0, buffer, size, None)

        if result > 0:
            return buffer[:result - 2]
        if GetLastError() != SystemErrorCode.ERROR_INSUFFICIENT_BUFFER:
            break

    return 'Unknown Error'