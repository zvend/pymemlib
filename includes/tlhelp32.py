from _ctypes import Structure, sizeof
from enum import Enum

from kernel_lib.defines import (
    LONG,
    DWORD,
    ULONG_PTR,
    HMODULE,
    CHAR,
    MAX_PATH,
 )


class ToolhelpFlags(Enum):
    """
    The portions of the system to be included in the snapshot.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/tlhelp32/nf-tlhelp32-createtoolhelp32snapshot
    """

    TH32CS_INHERIT      = 0x80000000
    TH32CS_SNAPHEAPLIST = 0x00000001
    TH32CS_SNAPPROCESS  = 0x00000002
    TH32CS_SNAPTHREAD   = 0x00000004
    TH32CS_SNAPMODULE   = 0x00000008
    TH32CS_SNAPMODULE32 = 0x00000010
    TH32CS_SNAPALL      = 0x000000FF

    def __get__(self, instance, owner):
        return self.value


class MODULEENTRY32(Structure):
    """
    Describes an entry from a list of the modules belonging to the specified process.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/tlhelp32/ns-tlhelp32-moduleentry32
    """

    _fields_ = [
        ('dwSize',              DWORD),
        ('th32ModuleID',        DWORD),
        ('th32ProcessID',       DWORD),
        ('GlblcntUsage',        DWORD),
        ('ProccntUsage',        DWORD),
        ('modBaseAddr',         ULONG_PTR),
        ('modBaseSize',         DWORD),
        ('hModule',             HMODULE),
        ('szModule',            CHAR * 256),
        ('szExePath',           CHAR * MAX_PATH)
    ]

    def __init__(self):
        self.dwSize = sizeof(self)


class PROCESSENTRY32(Structure):
    """
    Describes an entry from a list of the processes residing in the system address space when a snapshot was taken.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/tlhelp32/ns-tlhelp32-processentry32
    """

    _fields_ = [
        ('dwSize',              DWORD),
        ('cntUsage',            DWORD),
        ('th32ProcessID',       DWORD),
        ('th32DefaultHeapID',   ULONG_PTR),
        ('th32ModuleID',        DWORD),
        ('cntThreads',          DWORD),
        ('th32ParentProcessID', DWORD),
        ('pcPriClassBase',      LONG),
        ('dwFlags',             DWORD),
        ('szExeFile',           CHAR * MAX_PATH)
    ]

    def __init__(self):
        self.dwSize = sizeof(self)


class THREADENTRY32(Structure):
    """
    Describes an entry from a list of the threads executing in the system when a snapshot was taken.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/tlhelp32/ns-tlhelp32-threadentry32
    """

    _fields_ = [
        ('dwSize',              DWORD),
        ('cntUsage',            DWORD),
        ('th32ThreadID',        DWORD),
        ('th32OwnerProcessID',  DWORD),
        ('tpBasePri',           DWORD),
        ('tpDeltaPri',          DWORD),
        ('dwFlags',             DWORD)
    ]

    def __init__(self):
        self.dwSize = sizeof(self)
