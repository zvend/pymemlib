from _ctypes import Structure
from enum import Enum

from kernel_lib.defines import PVOID, DWORD, SIZE_T, ULONGLONG, BYTE, LONGLONG, DWORD64, WORD

# Constants
WOW64_SIZE_OF_80387_REGISTERS       = 80
WOW64_MAXIMUM_SUPPORTED_EXTENSION   = 512


class NTSTATUS(Enum):
    """
    By combining the NTSTATUS into a single 32-bit numbering space, the following
    NTSTATUS values are defined. Most values also have a defined default message
    that can be used to map the value to a human-readable text message. When this
    is done, the NTSTATUS value is also known as a message identifier.

    Refs: https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-erref/596a1078-e883-4972-9bbc-49e60bebca55
    """
    STATUS_WAIT_0                     = 0x00000000
    STATUS_ABANDONED_WAIT_0           = 0x00000080
    STATUS_USER_APC                   = 0x000000C0
    STATUS_TIMEOUT                    = 0x00000102
    STATUS_PENDING                    = 0x00000103
    DBG_EXCEPTION_HANDLED             = 0x00010001
    DBG_CONTINUE                      = 0x00010002
    STATUS_SEGMENT_NOTIFICATION       = 0x40000005
    STATUS_FATAL_APP_EXIT             = 0x40000015
    DBG_REPLY_LATER                   = 0x40010001
    DBG_TERMINATE_THREAD              = 0x40010003
    DBG_TERMINATE_PROCESS             = 0x40010004
    DBG_CONTROL_C                     = 0x40010005
    DBG_PRINTEXCEPTION_C              = 0x40010006
    DBG_RIPEXCEPTION                  = 0x40010007
    DBG_CONTROL_BREAK                 = 0x40010008
    DBG_COMMAND_EXCEPTION             = 0x40010009
    DBG_PRINTEXCEPTION_WIDE_C         = 0x4001000A
    STATUS_GUARD_PAGE_VIOLATION       = 0x80000001
    STATUS_DATATYPE_MISALIGNMENT      = 0x80000002
    STATUS_BREAKPOINT                 = 0x80000003
    STATUS_SINGLE_STEP                = 0x80000004
    STATUS_LONGJUMP                   = 0x80000026
    STATUS_UNWIND_CONSOLIDATE         = 0x80000029
    DBG_EXCEPTION_NOT_HANDLED         = 0x80010001
    STATUS_ACCESS_VIOLATION           = 0xC0000005
    STATUS_IN_PAGE_ERROR              = 0xC0000006
    STATUS_INVALID_HANDLE             = 0xC0000008
    STATUS_INVALID_PARAMETER          = 0xC000000D
    STATUS_NO_MEMORY                  = 0xC0000017
    STATUS_ILLEGAL_INSTRUCTION        = 0xC000001D
    STATUS_NONCONTINUABLE_EXCEPTION   = 0xC0000025
    STATUS_INVALID_DISPOSITION        = 0xC0000026
    STATUS_ARRAY_BOUNDS_EXCEEDED      = 0xC000008C
    STATUS_FLOAT_DENORMAL_OPERAND     = 0xC000008D
    STATUS_FLOAT_DIVIDE_BY_ZERO       = 0xC000008E
    STATUS_FLOAT_INEXACT_RESULT       = 0xC000008F
    STATUS_FLOAT_INVALID_OPERATION    = 0xC0000090
    STATUS_FLOAT_OVERFLOW             = 0xC0000091
    STATUS_FLOAT_STACK_CHECK          = 0xC0000092
    STATUS_FLOAT_UNDERFLOW            = 0xC0000093
    STATUS_INTEGER_DIVIDE_BY_ZERO     = 0xC0000094
    STATUS_INTEGER_OVERFLOW           = 0xC0000095
    STATUS_PRIVILEGED_INSTRUCTION     = 0xC0000096
    STATUS_STACK_OVERFLOW             = 0xC00000FD
    STATUS_DLL_NOT_FOUND              = 0xC0000135
    STATUS_ORDINAL_NOT_FOUND          = 0xC0000138
    STATUS_ENTRYPOINT_NOT_FOUND       = 0xC0000139
    STATUS_CONTROL_C_EXIT             = 0xC000013A
    STATUS_DLL_INIT_FAILED            = 0xC0000142
    STATUS_FLOAT_MULTIPLE_FAULTS      = 0xC00002B4
    STATUS_FLOAT_MULTIPLE_TRAPS       = 0xC00002B5
    STATUS_REG_NAT_CONSUMPTION        = 0xC00002C9
    STATUS_HEAP_CORRUPTION            = 0xC0000374
    STATUS_STACK_BUFFER_OVERRUN       = 0xC0000409
    STATUS_INVALID_CRUNTIME_PARAMETER = 0xC0000417
    STATUS_ASSERTION_FAILURE          = 0xC0000420
    STATUS_ENCLAVE_VIOLATION          = 0xC00004A2
    STATUS_INTERRUPTED                = 0xC0000515
    STATUS_THREAD_NOT_RUNNING         = 0xC0000516
    STATUS_ALREADY_REGISTERED         = 0xC0000718
    STATUS_SXS_EARLY_DEACTIVATION     = 0xC015000F
    STATUS_SXS_INVALID_DEACTIVATION   = 0xC0150010

    def __get__(self, instance, owner):
        return self.value


class ContextFlag(Enum):
    """
    Contains processor-specific register data. The system uses CONTEXT structures
    to perform various internal operations. Refer to the header file WinNT.h for
    definitions of this structure for each processor architecture.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-arm64_nt_context
    """
    CONTEXT_i386                = 0x00010000
    CONTEXT_CONTROL             = CONTEXT_i386 | 0x00000001    # SS:SP, CS:IP, FLAGS, BP
    CONTEXT_INTEGER             = CONTEXT_i386 | 0x00000002    # AX, BX, CX, DX, SI, DI
    CONTEXT_SEGMENTS            = CONTEXT_i386 | 0x00000004    # DS, ES, FS, GS
    CONTEXT_FLOATING_POINT      = CONTEXT_i386 | 0x00000008    # 387 state
    CONTEXT_DEBUG_REGISTERS     = CONTEXT_i386 | 0x00000010    # DB 0-3,6,7
    CONTEXT_EXTENDED_REGISTERS  = CONTEXT_i386 | 0x00000020    # cpu specific extensions
    CONTEXT_FULL                = CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_SEGMENTS
    CONTEXT_ALL                 = CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_SEGMENTS | \
                                  CONTEXT_FLOATING_POINT | CONTEXT_DEBUG_REGISTERS | \
                                  CONTEXT_EXTENDED_REGISTERS
    CONTEXT_XSTATE              = CONTEXT_i386 | 0x00000040

    CONTEXT_EXCEPTION_ACTIVE    = 0x08000000
    CONTEXT_SERVICE_ACTIVE      = 0x10000000
    CONTEXT_EXCEPTION_REQUEST   = 0x40000000
    CONTEXT_EXCEPTION_REPORTING = 0x80000000

    def __get__(self, instance, owner):
        return self.value


class StandardAccessRight(Enum):
    """
    The following are masks for the predefined standard access types.

    Refs: https://docs.microsoft.com/en-us/windows/win32/secauthz/standard-access-rights?redirectedfrom=MSDN
    """

    SPECIFIC_RIGHTS_ALL         = 0x0000FFFF
    DELETE                      = 0x00010000
    READ_CONTROL                = 0x00020000
    WRITE_DAC                   = 0x00040000
    WRITE_OWNER                 = 0x00080000
    STANDARD_RIGHTS_REQUIRED    = 0x000F0000
    SYNCHRONIZE                 = 0x00100000
    STANDARD_RIGHTS_ALL         = 0x001F0000
    ACCESS_SYSTEM_SECURITY      = 0x01000000
    MAXIMUM_ALLOWED             = 0x02000000
    GENERIC_ALL                 = 0x10000000
    GENERIC_EXECUTE             = 0x20000000
    GENERIC_WRITE               = 0x40000000
    GENERIC_READ                = 0x80000000

    def __get__(self, instance, owner):
        return self.value


class ProcessAccessRight(Enum):
    """
    The following are masks for the process-specific access rights.

    Refs: https://docs.microsoft.com/en-us/windows/win32/procthread/process-security-and-access-rights
    """

    PROCESS_TERMINATE                   = 0x0001
    PROCESS_CREATE_THREAD               = 0x0002
    PROCESS_SET_SESSIONID               = 0x0004
    PROCESS_VM_OPERATION                = 0x0008
    PROCESS_VM_READ                     = 0x0010
    PROCESS_VM_WRITE                    = 0x0020
    PROCESS_DUP_HANDLE                  = 0x0040
    PROCESS_CREATE_PROCESS              = 0x0080
    PROCESS_SET_QUOTA                   = 0x0100
    PROCESS_SET_INFORMATION             = 0x0200
    PROCESS_QUERY_INFORMATION           = 0x0400
    PROCESS_SUSPEN_RESUME               = 0x0800
    PROCESS_QUERY_LIMITED_INFORMATION   = 0x1000
    PROCESS_SET_LIMITED_INFORMATION     = 0x2000
    PROCESS_ALL_ACCESS                  = StandardAccessRight.STANDARD_RIGHTS_REQUIRED | \
                                          StandardAccessRight.SYNCHRONIZE | \
                                          0xFFFF

    def __get__(self, instance, owner):
        return self.value


class ThreadAccessRight(Enum):
    """
    The following table lists the thread-specific access rights.

    Refs: https://docs.microsoft.com/en-us/windows/win32/procthread/thread-security-and-access-rights
    """

    THREAD_TERMINATE                    = 0x0001
    THREAD_SUSPEND_RESUME               = 0x0002
    THREAD_GET_CONTEXT                  = 0x0008
    THREAD_SET_CONTEXT                  = 0x0010
    THREAD_SET_INFORMATION              = 0x0020
    THREAD_QUERY_INFORMATION            = 0x0040
    THREAD_SET_THREAD_TOKEN             = 0x0080
    THREAD_IMPERSONATE                  = 0x0100
    THREAD_DIRECT_IMPERSONATION         = 0x0200
    THREAD_SET_LIMITED_INFORMATION      = 0x0400
    THREAD_QUERY_LIMITED_INFORMATION    = 0x0800
    THREAD_RESUME                       = 0x1000
    THREAD_ALL_ACCESS                   = StandardAccessRight.STANDARD_RIGHTS_REQUIRED | \
                                          StandardAccessRight.SYNCHRONIZE | \
                                          0xFFFF

    def __get__(self, instance, owner):
        return self.value


class MemoryProtection(Enum):

    PAGE_NOACCESS                   = 0x01
    PAGE_READONLY                   = 0x02
    PAGE_READWRITE                  = 0x04
    PAGE_WRITECOPY                  = 0x08
    PAGE_EXECUTE                    = 0x10
    PAGE_EXECUTE_READ               = 0x20
    PAGE_EXECUTE_READWRITE          = 0x40
    PAGE_EXECUTE_WRITECOPY          = 0x80
    PAGE_GUARD                      = 0x100
    PAGE_NOCACHE                    = 0x200
    PAGE_WRITECOMBINE               = 0x400
    PAGE_GRAPHICS_NOACCESS          = 0x0800
    PAGE_GRAPHICS_READONLY          = 0x1000
    PAGE_GRAPHICS_READWRITE         = 0x2000
    PAGE_GRAPHICS_EXECUTE           = 0x4000
    PAGE_GRAPHICS_EXECUTE_READ      = 0x8000
    PAGE_GRAPHICS_EXECUTE_READWRITE = 0x10000
    PAGE_GRAPHICS_COHERENT          = 0x20000
    PAGE_ENCLAVE_THREAD_CONTROL     = 0x80000000
    PAGE_REVERT_TO_FILE_MAP         = 0x80000000
    PAGE_TARGETS_NO_UPDATE          = 0x40000000
    PAGE_TARGETS_INVALID            = 0x40000000
    PAGE_ENCLAVE_UNVALIDATED        = 0x20000000
    PAGE_ENCLAVE_DECOMMIT           = 0x10000000

    def __get__(self, instance, owner):
        return self.value


class MemoryAllocationType(Enum):

    MEM_UNMAP_WITH_TRANSIENT_BOOST  = 0x00000001
    MEM_PRESERVE_PLACEHOLDER        = 0x00000002
    MEM_COMMIT                      = 0x00001000
    MEM_RESERVE                     = 0x00002000
    MEM_DECOMMIT                    = 0x00004000
    MEM_RELEASE                     = 0x00008000
    MEM_FREE                        = 0x00010000
    MEM_RESERVE_PLACEHOLDER         = 0x00040000
    MEM_RESET                       = 0x00080000
    MEM_TOP_DOWN                    = 0x00100000
    MEM_WRITE_WATCH                 = 0x00200000
    MEM_PHYSICAL                    = 0x00400000
    MEM_ROTATE                      = 0x00800000
    MEM_DIFFERENT_IMAGE_BASE_OK     = 0x00800000
    MEM_RESET_UNDO                  = 0x01000000
    MEM_LARGE_PAGES                 = 0x20000000
    MEM_4MB_PAGES                   = 0x80000000
    MEM_64K_PAGES                   = MEM_LARGE_PAGES | MEM_PHYSICAL

    def __get__(self, instance, owner):
        return self.value


class MEMORY_BASIC_INFORMATION(Structure):
    """
    Contains information about a range of pages in the virtual address space of a process.
    The VirtualQuery and VirtualQueryEx functions use this structure. 32 bit version.

    VirtualQuery: https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualquery
    VirtualQueryEx: https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualqueryex

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-memory_basic_information
    """

    _pack_ = 4
    _fields_ = [
        ('BaseAddress',         PVOID),
        ('AllocationBase',      PVOID),
        ('AllocationProtect',   DWORD),
        ('RegionSize',          SIZE_T),
        ('State',               DWORD),
        ('Protect',             DWORD),
        ('Type',                DWORD)
    ]


class MEMORY_BASIC_INFORMATION64(Structure):
    """
    Contains information about a range of pages in the virtual address space of a process.
    The VirtualQuery and VirtualQueryEx functions use this structure. 64 bit version.

    VirtualQuery: https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualquery
    VirtualQueryEx: https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualqueryex

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-memory_basic_information
    """

    _pack_ = 8
    _fields_ = [
        ('BaseAddress',         ULONGLONG),
        ('AllocationBase',      ULONGLONG),
        ('AllocationProtect',   DWORD),
        ('__alignment1',        DWORD),
        ('RegionSize',          ULONGLONG),
        ('State',               DWORD),
        ('Protect',             DWORD),
        ('Type',                DWORD),
        ('__alignment2',        DWORD)      # can be seen in winnt.h
    ]


class WOW64_FLOATING_SAVE_AREA(Structure):
    """
    Represents the 80387 save area on WOW64. Refer to the header file WinNT.h
    for the definition of this structure.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-wow64_floating_save_area
    """
    _fields_ = [
        ('ControlWord',         DWORD),
        ('StatusWord',          DWORD),
        ('TagWord',             DWORD),
        ('ErrorOffset',         DWORD),
        ('ErrorSelector',       DWORD),
        ('DataOffset',          DWORD),
        ('DataSelector',        DWORD),
        ('RegisterArea',        BYTE * WOW64_SIZE_OF_80387_REGISTERS),
        ('Spare0',              DWORD)
    ]


class WOW64_CONTEXT(Structure):
    """
    The flags values within this flag control the contents of
    a CONTEXT record.

    If the context record is used as an input parameter, then
    for each portion of the context record controlled by a flag
    whose value is set, it is assumed that that portion of the
    context record contains valid context. If the context record
    is being used to modify a threads context, then only that
    portion of the threads context will be modified.

    If the context record is used as an IN OUT parameter to capture
    the context of a thread, then only those portions of the thread's
    context corresponding to set flags will be returned.

    The context record is never used as an OUT only parameter.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-wow64_context
    """
    _fields_ = [
        ('ContextFlag',         DWORD),
        ('Dr0',                 DWORD),
        ('Dr1',                 DWORD),
        ('Dr2',                 DWORD),
        ('Dr3',                 DWORD),
        ('Dr6',                 DWORD),
        ('Dr7',                 DWORD),
        ('FloatSave',           WOW64_FLOATING_SAVE_AREA),
        ('SegGs',               DWORD),
        ('SegFs',               DWORD),
        ('SegEs',               DWORD),
        ('SegDs',               DWORD),
        ('Edi',                 DWORD),
        ('Esi',                 DWORD),
        ('Ebx',                 DWORD),
        ('Edx',                 DWORD),
        ('Ecx',                 DWORD),
        ('Eax',                 DWORD),
        ('Ebp',                 DWORD),
        ('Eip',                 DWORD),
        ('SegCs',               DWORD),
        ('EFlags',              DWORD),
        ('Esp',                 DWORD),
        ('SegSs',               DWORD),
        ('ExtendedRegisters',   BYTE * WOW64_MAXIMUM_SUPPORTED_EXTENSION)
    ]


class M128A(Structure):
    """
    Define 128-bit 16-byte aligned xmm register type.

    Refs: winnt.h _M128A Line 2575
    """
    _fields_ = [
        ('Low',                 ULONGLONG),
        ('High',                LONGLONG)
    ]


class FLOATING_SAVE_AREA64(Structure):
    """
    Union struct for CONTEXT64.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-context
    Refs: winnt.h XMM_SAVE_AREA32 "Floating point state" Line 3960
    """
    _fields_ = [
        ('Header',              M128A * 2),
        ('Legacy',              M128A * 8),
        ('Xmm0',                M128A),
        ('Xmm1',                M128A),
        ('Xmm2',                M128A),
        ('Xmm3',                M128A),
        ('Xmm4',                M128A),
        ('Xmm5',                M128A),
        ('Xmm6',                M128A),
        ('Xmm7',                M128A),
        ('Xmm8',                M128A),
        ('Xmm9',                M128A),
        ('Xmm10',               M128A),
        ('Xmm11',               M128A),
        ('Xmm12',               M128A),
        ('Xmm13',               M128A),
        ('Xmm14',               M128A),
        ('Xmm15',               M128A)
    ]


class CONTEXT64(Structure):
    """
    Contains processor-specific register data. The system uses CONTEXT structures
    to perform various internal operations. Refer to the header file WinNT.h for
    definitions of this structure for each processor architecture.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-context
    """

    _fields_ = [
        ('P1Home',				DWORD64),
        ('P2Home',				DWORD64),
        ('P3Home',				DWORD64),
        ('P4Home',				DWORD64),
        ('P5Home',				DWORD64),
        ('P6Home',				DWORD64),
        ('ContextFlag',		    DWORD),
        ('MxCsr',				DWORD),
        ('SegCs',				WORD),
        ('SegDs',				WORD),
        ('SegEs',				WORD),
        ('SegFs',				WORD),
        ('SegGs',				WORD),
        ('SegSs',				WORD),
        ('EFlags',				DWORD),
        ('Dr0',				    DWORD64),
        ('Dr1',				    DWORD64),
        ('Dr2',				    DWORD64),
        ('Dr3',				    DWORD64),
        ('Dr6',				    DWORD64),
        ('Dr7',				    DWORD64),
        ('Rax',				    DWORD64),
        ('Rcx',				    DWORD64),
        ('Rdx',				    DWORD64),
        ('Rbx',				    DWORD64),
        ('Rsp',				    DWORD64),
        ('Rbp',				    DWORD64),
        ('Rsi',				    DWORD64),
        ('Rdi',				    DWORD64),
        ('R8',				    DWORD64),
        ('R9',				    DWORD64),
        ('R10',				    DWORD64),
        ('R11',				    DWORD64),
        ('R12',				    DWORD64),
        ('R13',				    DWORD64),
        ('R14',				    DWORD64),
        ('R15',				    DWORD64),
        ('Rip',				    DWORD64),
        ('FltSave',		        FLOATING_SAVE_AREA64),
        ('VectorRegister',		M128A * 26),
        ('VectorControl',		DWORD64),
        ('DebugControl',		DWORD64),
        ('LastBranchToRip',		DWORD64),
        ('LastBranchFromRip',	DWORD64),
        ('LastExceptionToRip',	DWORD64),
        ('LastExceptionFromRip',DWORD64)
    ]
