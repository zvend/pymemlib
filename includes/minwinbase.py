import ctypes
from _ctypes import Structure
from enum import Enum

from kernel_lib.defines import (
    DWORD,
    LPVOID,
    PVOID,
    ULONG_PTR,
    HANDLE,
    WORD,
    LPSTR
)


class DebugEventCode(Enum):
    """
    The code that identifies the type of debugging event.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/minwinbase/ns-minwinbase-debug_event
    """

    EXCEPTION_DEBUG_EVENT       = 1
    CREATE_THREAD_DEBUG_EVENT   = 2
    CREATE_PROCESS_DEBUG_EVENT  = 3
    EXIT_THREAD_DEBUG_EVENT     = 4
    EXIT_PROCESS_DEBUG_EVENT    = 5
    LOAD_DLL_DEBUG_EVENT        = 6
    UNLOAD_DLL_DEBUG_EVENT      = 7
    OUTPUT_DEBUG_STRING_EVENT   = 8
    RIP_EVENT                   = 9

    def __get__(self, instance, owner):
        return self.value


class EXCEPTION_DEBUG_INFO(Structure):
    """
    Contains exception information that can be used by a debugger.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/minwinbase/ns-minwinbase-exception_debug_info
    """

    EXCEPTION_MAXIMUM_PARAMETERS = 15
    _fields_ = [
        ('ExceptionCode',           DWORD),
        ('ExceptionFlags',          DWORD),
        ('ExceptionRecord',         LPVOID),
        ('ExceptionAddress',        PVOID),
        ('NumberParameters',        DWORD),
        ('ExceptionInformation',    ULONG_PTR * 15),
        ('dwFirstChance',           DWORD)
    ]


class CREATE_THREAD_DEBUG_INFO(Structure):
    """
    Contains thread-creation information that can be used by a debugger.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/minwinbase/ns-minwinbase-create_thread_debug_info
    """

    _fields_ = [
        ('hThread',                 HANDLE),
        ('lpThreadLocalBase',       LPVOID),
        ('lpStartAddress',          LPVOID)
    ]


class CREATE_PROCESS_DEBUG_INFO(Structure):
    """
    Contains process creation information that can be used by a debugger.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/minwinbase/ns-minwinbase-create_process_debug_info
    """

    _fields_ = [
        ('hFile',                   HANDLE),
        ('hProcess',                HANDLE),
        ('hThread',                 HANDLE),
        ('lpBaseOfImage',           LPVOID),
        ('dwDebugInfoFileOffset',   DWORD),
        ('nDebugInfoSize',          DWORD),
        ('lpThreadLocalBase',       LPVOID),
        ('lpStartAddress',          LPVOID),
        ('lpImageName',             LPVOID),
        ('fUnicode',                WORD)
    ]


class EXIT_THREAD_DEBUG_INFO(Structure):
    """
    Contains the exit code for a terminating thread.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/minwinbase/ns-minwinbase-exit_thread_debug_info
    """

    _fields_ = [
        ('dwExitCode',              DWORD)
    ]


class EXIT_PROCESS_DEBUG_INFO(Structure):
    """
    Contains the exit code for a terminating process.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/minwinbase/ns-minwinbase-exit_process_debug_info
    """

    _fields_ = [
        ('dwExitCode',              DWORD)
    ]


class LOAD_DLL_DEBUG_INFO(Structure):
    """
    Contains information about a dynamic-link library (DLL) that has just been loaded.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/minwinbase/ns-minwinbase-load_dll_debug_info
    """

    _fields_ = [
        ('hFile',                   HANDLE),
        ('lpBaseOfDll',             LPVOID),
        ('dwDebugInfoFileOffset',   DWORD),
        ('nDebugInfoSize',          DWORD),
        ('lpImageName',             LPVOID),
        ('fUnicode',                WORD)
    ]


class UNLOAD_DLL_DEBUG_INFO(Structure):
    """
    Contains information about a dynamic-link library (DLL) that has just been unloaded.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/minwinbase/ns-minwinbase-unload_dll_debug_info
    """
    _fields_ = [
        ('lpBaseOfDll',             LPVOID)
    ]


class OUTPUT_DEBUG_STRING_INFO(Structure):
    """
    Contains the address, format, and length, in bytes, of a debugging string.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/minwinbase/ns-minwinbase-output_debug_string_info
    """
    _fields_ = [
        ('lpDebugStringData',       LPSTR),
        ('fUnicode',                WORD),
        ('nDebugStringLength',      WORD)
    ]


class RIP_INFO(Structure):
    """
    Contains the error that caused the RIP debug event.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/minwinbase/ns-minwinbase-rip_info
    """
    _fields_ = [
        ('dwError',                 DWORD),
        ('dwType',                  DWORD)
    ]


class UNION_DEBUG_EVENT(ctypes.Union):
    """
    Any additional information relating to the debugging event. This union
    takes on the type and value appropriate to the type of debugging event,
    as described in the dwDebugEventCode member.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/minwinbase/ns-minwinbase-debug_event
    """

    _fields_ = [
        ('Exception',               EXCEPTION_DEBUG_INFO),
        ('CreateThread',            CREATE_THREAD_DEBUG_INFO),
        ('CreateProcessInfo',       CREATE_PROCESS_DEBUG_INFO),
        ('ExitThread',              EXIT_THREAD_DEBUG_INFO),
        ('ExitProcess',             EXIT_PROCESS_DEBUG_INFO),
        ('LoadDll',                 LOAD_DLL_DEBUG_INFO),
        ('UnloadDll',               UNLOAD_DLL_DEBUG_INFO),
        ('DebugString',             OUTPUT_DEBUG_STRING_INFO),
        ('RipInfo',                 RIP_INFO)
    ]


class DEBUG_EVENT(Structure):
    """
    Describes a debugging event.

    Refs: https://docs.microsoft.com/en-us/windows/win32/api/minwinbase/ns-minwinbase-debug_event
    """

    _fields_ = [
        ('dwDebugEventCode',        DWORD),
        ('dwProcessId',             DWORD),
        ('dwThreadId',              DWORD),
        ('u',                       UNION_DEBUG_EVENT)
    ]
