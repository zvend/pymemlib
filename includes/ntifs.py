from enum import Enum


class THREADINFOCLASS(Enum):
    """
    The THREADINFOCLASS is an enumeration whose values are intended as
    input to the ZwQueryInformationThread and ZwSetInformationThread functions.
    Different values select different types of information to query or set.

    Refs: https://www.geoffchappell.com/studies/windows/km/ntoskrnl/api/ps/psquery/class.htm
    """

    # Symbolic Name                 NumericValue    Versions
    ThreadBasicInformation          = 0x00          # all
    ThreadTimes                     = 0x01          # all
    ThreadPriority                  = 0x02          # all
    ThreadBasePriority              = 0x03          # all
    ThreadAffinityMask              = 0x04          # all
    ThreadImpersonationToken        = 0x05          # all
    ThreadDescriptorTableEntry      = 0x06          # all
    ThreadEnableAlignmentFaultFixup = 0x07          # all
    ThreadEventPair_Reusable        = 0x08          # 5.0 and higher
    ThreadQuerySetWin32StartAddress = 0x09          # all
    ThreadZeroTlsCell               = 0x0A          # all
    ThreadPerformanceCount          = 0x0B          # 3.51 and higher
    ThreadAmILastThread             = 0x0C          # 3.51 and higher
    ThreadIdealProcessor            = 0x0D          # 4.0 and higher
    ThreadPriorityBoost             = 0x0E          # 4.0 and higher
    ThreadSetTlsArrayAddress        = 0x0F          # 4.0 and higher
    ThreadIsIoPending               = 0x10          # 5.0 and higher
    ThreadHideFromDebugger          = 0x11          # 5.0 and higher
    ThreadBreakOnTermination        = 0x12          # 5.2 and higher
    ThreadSwitchLegacyState         = 0x13          # 5.2 from Windows Server 2003 SP1, and higher
    ThreadIsTerminated              = 0x14          # 5.2 from Windows Server 2003 SP1, and higher
    ThreadLastSystemCall            = 0x15          # 6.0 and higher
    ThreadIoPriority                = 0x16          # 6.0 and higher
    ThreadCycleTime                 = 0x17          # 6.0 and higher
    ThreadPagePriority              = 0x18          # 6.0 and higher
    ThreadActualBasePriority        = 0x19          # 6.0 and higher
    ThreadTebInformation            = 0x1A          # 6.0 and higher
    ThreadCSwitchMon                = 0x1B          # 6.0 and higher
    ThreadCSwitchPmu                = 0x1C          # 6.1 and higher
    ThreadWow64Context              = 0x1D          # 6.1 and higher
    ThreadGroupInformation          = 0x1E          # 6.1 and higher
    ThreadUmsInformation            = 0x1F          # 6.1 and higher
    ThreadCounterProfiling          = 0x20          # 6.1 and higher
    ThreadIdealProcessorEx          = 0x21          # 6.1 and higher
    ThreadCpuAccountingInformation  = 0x22          # 6.2 and higher
    ThreadSuspendCount              = 0x23          # 6.3 and higher
    ThreadHeterogeneousCpuPolicy    = 0x24          # 10.0 and higher
    ThreadContainerId               = 0x25          # 10.0 and higher
    ThreadNameInformation           = 0x26          # 10.0 and higher
    ThreadSelectedCpuSets           = 0x27          # 10.0 and higher
    ThreadSystemThreadInformation   = 0x28          # 10.0 and higher
    ThreadActualGroupAffinity       = 0x29          # 10.0 and higher

    def __get__(self, instance, owner):
        return self.value