from _ctypes import Structure

from kernel_lib.defines import HANDLE


class CLIENT_ID(Structure):
    """
    The CLIENT_ID structure contains identifiers of a process and a thread.

    Refs: https://processhacker.sourceforge.io/doc/struct___c_l_i_e_n_t___i_d.html
    Refs: https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-tsts/a11e7129-685b-4535-8d37-21d4596ac057
    """

    _fields_ = [
        ('UniqueProcess',       HANDLE),
        ('UniqueThread',        HANDLE)
    ]