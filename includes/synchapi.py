# Refs: https://docs.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-waitforsingleobject

# Parameter milliseconds indicator
IGNORE                              = 0x00000000
INFINITE                            = 0xFFFFFFFF

# WaitForSingleObject return value indicators
WAIT_OBJECT_0                       = 0x00000000
WAIT_ABANDONED                      = 0x00000080
WAIT_TIMEOUT                        = 0x00000102
WAIT_FAILED                         = 0xFFFFFFFF
