from _ctypes import Structure

from kernel_lib.defines import DWORD, LPVOID, ULONG_PTR, LONG
from kernel_lib.includes.ntbasic import CLIENT_ID


class THREAD_BASIC_INFORMATION(Structure):
    """
    Contains basic information about a thread.

    Refs: https://processhacker.sourceforge.io/doc/struct___t_h_r_e_a_d___b_a_s_i_c___i_n_f_o_r_m_a_t_i_o_n.html
    """

    _fields_ = [
        ('ExitStatus',              DWORD),
        ('TebBaseAddress',          LPVOID),
        ('ClientId',                CLIENT_ID),
        ('AffinityMask',            ULONG_PTR),
        ('Priority',                LONG),
        ('BasePriority',            LONG)
    ]