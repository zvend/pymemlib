import sys
from ctypes import (
    POINTER, WinDLL,
    c_long, c_longlong, c_ulong, c_ulonglong, c_float,
    c_byte, c_char, c_ushort, c_uint, c_size_t, c_wchar,
    c_char_p, c_wchar_p, c_void_p
)



# General
PYTHON_IS_64_BITS   = sys.maxsize > 2 ** 32
MAX_PATH            = 260

# Make kernel32 independent of windll which may be unpredictable.
kernel32    = WinDLL('kernel32.dll')
ntdll       = WinDLL('ntdll.dll')

# Variable types
BOOL        = c_long
BYTE        = c_byte
CHAR        = c_char
DWORD       = c_ulong
DWORD64     = c_ulonglong
FLOAT       = c_float
LONG        = c_long
LONGLONG    = c_longlong
LPSTR       = c_char_p
LPVOID      = c_void_p
LPWSTR      = c_wchar_p
PVOID       = c_void_p
SIZE_T      = c_size_t
UINT        = c_uint
ULONG       = c_ulong
ULONG_PTR   = SIZE_T
ULONGLONG   = c_ulonglong
WCHAR       = c_wchar
WORD        = c_ushort
HANDLE      = c_void_p
HMODULE     = HANDLE
NTSTATUS    = LONG
PBOOL       = POINTER(BOOL)
PDWORD      = POINTER(DWORD)
PDWORD64    = POINTER(DWORD64)
PHANDLE     = POINTER(HANDLE)
PULONG      = POINTER(ULONG)
