from _ctypes import POINTER

from kernel_lib.defines import CHAR, kernel32, HANDLE, BOOL, PYTHON_IS_64_BITS, PHANDLE, DWORD, UINT, LPVOID, SIZE_T, \
    PDWORD, PBOOL, LPWSTR, LPSTR, HMODULE, ntdll, PVOID, ULONG, PULONG, NTSTATUS
from kernel_lib.includes.minwinbase import DEBUG_EVENT
from kernel_lib.includes.tlhelp32 import MODULEENTRY32, PROCESSENTRY32, THREADENTRY32
from kernel_lib.includes.winnt import CONTEXT64, WOW64_CONTEXT


def create_buffer(size):
    """Create a ctypes buffer of a given size."""

    bufftype = (CHAR * size)
    return bufftype()


CONTEXT                             = CONTEXT64 if PYTHON_IS_64_BITS else WOW64_CONTEXT

CloseHandle                         = kernel32.CloseHandle
CloseHandle.argtypes                = [HANDLE]
CloseHandle.restype                 = BOOL

DuplicateHandle                     = kernel32.DuplicateHandle
DuplicateHandle.argtypes            = [HANDLE, HANDLE, HANDLE, PHANDLE, DWORD, BOOL, DWORD]
DuplicateHandle.restype             = BOOL

GetLastError                        = kernel32.GetLastError
GetLastError.argtypes               = []
GetLastError.restype                = DWORD

WaitForSingleObject                 = kernel32.WaitForSingleObject


OpenProcess                         = kernel32.OpenProcess
OpenProcess.argtypes                = [DWORD, BOOL, DWORD]
OpenProcess.restype                 = HANDLE

TerminateProcess                    = kernel32.TerminateProcess
TerminateProcess.argtypes           = [HANDLE, UINT]
TerminateProcess.restype            = BOOL

OpenThread                          = kernel32.OpenThread
OpenThread.argtypes                 = [DWORD, BOOL, DWORD]
OpenThread.restype                  = HANDLE

GetThreadId                         = kernel32.GetThreadId
GetThreadId.argtypes                = [HANDLE]
GetThreadId.restype                 = DWORD

ResumeThread                        = kernel32.ResumeThread
ResumeThread.argtypes               = [HANDLE]
ResumeThread.restype                = DWORD

SuspendThread                       = kernel32.SuspendThread
SuspendThread.argtypes              = [HANDLE]
SuspendThread.restype               = DWORD

CreateRemoteThread                  = kernel32.CreateRemoteThread
CreateRemoteThread.argtypes         = [HANDLE, LPVOID, SIZE_T, LPVOID, LPVOID, DWORD, PDWORD]
CreateRemoteThread.restype          = HANDLE

TerminateThread                     = kernel32.TerminateThread
TerminateThread.argtypes            = [HANDLE, DWORD]
TerminateThread.restype             = BOOL

GetExitCodeThread                   = kernel32.GetExitCodeThread
GetExitCodeThread.argtypes          = [HANDLE, PDWORD]
GetExitCodeThread.restype           = BOOL

GetExitCodeProcess                  = kernel32.GetExitCodeProcess
GetExitCodeProcess.argtypes         = [HANDLE, PDWORD]
GetExitCodeProcess.restype          = BOOL

IsWow64Process                      = kernel32.IsWow64Process
IsWow64Process.argtypes             = [HANDLE, PBOOL]
IsWow64Process.restype              = BOOL

GetThreadContext                    = kernel32.GetThreadContext
GetThreadContext.argtypes           = [HANDLE, POINTER(CONTEXT)]
GetThreadContext.restype            = BOOL

SetThreadContext                    = kernel32.SetThreadContext
SetThreadContext.argtypes           = [HANDLE, POINTER(CONTEXT)]
SetThreadContext.restype            = BOOL

Wow64GetThreadContext               = kernel32.Wow64GetThreadContext
Wow64GetThreadContext.argtypes      = [HANDLE, POINTER(WOW64_CONTEXT)]
Wow64GetThreadContext.restype       = BOOL


VirtualAllocEx                      = kernel32.VirtualAllocEx
VirtualAllocEx.argtypes             = [HANDLE, LPVOID, SIZE_T, DWORD, DWORD]
VirtualAllocEx.restype              = LPVOID

VirtualFreeEx                       = kernel32.VirtualFreeEx
VirtualFreeEx.argtypes              = [HANDLE, LPVOID, SIZE_T, DWORD]
VirtualFreeEx.restype               = BOOL

VirtualProtectEx                    = kernel32.VirtualProtectEx
VirtualProtectEx.argtypes           = [HANDLE, LPVOID, SIZE_T, DWORD, PDWORD]
VirtualProtectEx.restype            = BOOL

VirtualQueryEx                      = kernel32.VirtualQueryEx
VirtualQueryEx.argtypes             = [HANDLE, LPVOID, LPVOID, SIZE_T]
VirtualQueryEx.restype              = SIZE_T

ReadProcessMemory                   = kernel32.ReadProcessMemory
ReadProcessMemory.argtypes          = [HANDLE, LPVOID, LPVOID, SIZE_T, POINTER(SIZE_T)]
ReadProcessMemory.restype           = BOOL

WriteProcessMemory                  = kernel32.WriteProcessMemory
WriteProcessMemory.argtypes         = [HANDLE, LPVOID, LPVOID, SIZE_T, POINTER(SIZE_T)]

FlushInstructionCache               = kernel32.FlushInstructionCache
FlushInstructionCache.argtypes      = [HANDLE, LPVOID, SIZE_T]
FlushInstructionCache.restype       = BOOL

CreateToolhelp32Snapshot            = kernel32.CreateToolhelp32Snapshot
CreateToolhelp32Snapshot.argtypes   = [DWORD, DWORD]
CreateToolhelp32Snapshot.restype    = HANDLE

Module32First                       = kernel32.Module32First
Module32First.argtypes              = [HANDLE, POINTER(MODULEENTRY32)]
Module32First.restype               = BOOL

Module32Next                        = kernel32.Module32Next
Module32Next.argtypes               = [HANDLE, POINTER(MODULEENTRY32)]
Module32Next.restype                = BOOL

Process32First                      = kernel32.Process32First
Process32First.argtypes             = [HANDLE, POINTER(PROCESSENTRY32)]
Process32First.restype              = BOOL

Process32Next                       = kernel32.Process32Next
Process32Next.argtypes              = [HANDLE, POINTER(PROCESSENTRY32)]
Process32Next.restype               = BOOL

Thread32First                       = kernel32.Thread32First
Thread32First.argtypes              = [HANDLE, POINTER(THREADENTRY32)]
Thread32First.restype               = BOOL

Thread32Next                        = kernel32.Thread32Next
Thread32Next.argtypes               = [HANDLE, POINTER(THREADENTRY32)]
Thread32Next.restype                = BOOL


DebugSetProcessKillOnExit           = kernel32.DebugSetProcessKillOnExit
DebugSetProcessKillOnExit.argtypes  = [BOOL]
DebugSetProcessKillOnExit.restype   = BOOL

DebugActiveProcess                  = kernel32.DebugActiveProcess
DebugActiveProcess.argtypes         = [DWORD]
DebugActiveProcess.restype          = BOOL

DebugActiveProcessStop              = kernel32.DebugActiveProcessStop
DebugActiveProcessStop.argtypes     = [DWORD]
DebugActiveProcessStop.restype      = BOOL

WaitForDebugEvent                   = kernel32.WaitForDebugEvent
WaitForDebugEvent.argtypes          = [POINTER(DEBUG_EVENT), DWORD]
WaitForDebugEvent.restype           = BOOL

ContinueDebugEvent                  = kernel32.ContinueDebugEvent
ContinueDebugEvent.argtypes         = [DWORD, DWORD, DWORD]
ContinueDebugEvent.restype          = BOOL


FormatMessageW                      = kernel32.FormatMessageW
FormatMessageW.argtypes             = [DWORD, LPVOID, DWORD, DWORD, LPWSTR, DWORD, LPVOID]
FormatMessageW.restype              = DWORD

LoadLibraryA                        = kernel32.LoadLibraryA
LoadLibraryA.argtypes               = [LPSTR]
LoadLibraryA.restype                = HMODULE

LoadLibraryW                        = kernel32.LoadLibraryW
LoadLibraryW.argtypes               = [LPWSTR]
LoadLibraryW.restype                = HMODULE

GetProcAddress                      = kernel32.GetProcAddress
GetProcAddress.argtypes             = [HMODULE, LPSTR]
GetProcAddress.restype              = LPVOID

GetModuleHandleA                    = kernel32.GetModuleHandleA
GetModuleHandleA.argtypes           = [LPSTR]
GetModuleHandleA.restype            = HMODULE

GetModuleHandleW                    = kernel32.GetModuleHandleW
GetModuleHandleW.argtypes           = [LPWSTR]
GetModuleHandleW.restype            = HMODULE


NtQueryInformationThread            = ntdll.NtQueryInformationThread
NtQueryInformationThread.argtypes   = [HANDLE, DWORD, PVOID, ULONG, PULONG]
NtQueryInformationThread.restype    = NTSTATUS
