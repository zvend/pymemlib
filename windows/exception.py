from enum import Enum

from kernel_lib.functions import GetLastError
from kernel_lib.includes.winbase import FormatMessage
from kernel_lib.includes.winnt import NTSTATUS as WINNT


class ExceptionCode(Enum):
    """
    The value identifies the type of exception. The following table identifies
    the exception codes that can occur due to common programming errors. These
    values are defined in WinBase.h and WinNT.h.

    Refs: https://docs.microsoft.com/en-us/windows/win32/debug/getexceptioncode
    """

    EXCEPTION_DATATYPE_MISALIGNMENT     = WINNT.STATUS_DATATYPE_MISALIGNMENT
    EXCEPTION_BREAKPOINT                = WINNT.STATUS_BREAKPOINT
    EXCEPTION_SINGLE_STEP               = WINNT.STATUS_SINGLE_STEP
    EXCEPTION_ACCESS_VIOLATION          = WINNT.STATUS_ACCESS_VIOLATION
    EXCEPTION_IN_PAGE_ERROR             = WINNT.STATUS_IN_PAGE_ERROR
    EXCEPTION_ILLEGAL_INSTRUCTION       = WINNT.STATUS_ILLEGAL_INSTRUCTION
    EXCEPTION_NONCONTINUABLE_EXCEPTION  = WINNT.STATUS_NONCONTINUABLE_EXCEPTION
    EXCEPTION_INVALID_DISPOSITION       = WINNT.STATUS_INVALID_DISPOSITION
    EXCEPTION_ARRAY_BOUNDS_EXCEEDED     = WINNT.STATUS_ARRAY_BOUNDS_EXCEEDED
    EXCEPTION_FLT_DENORMAL_OPERAND      = WINNT.STATUS_FLOAT_DENORMAL_OPERAND
    EXCEPTION_FLT_DIVIDE_BY_ZERO        = WINNT.STATUS_FLOAT_DIVIDE_BY_ZERO
    EXCEPTION_FLT_INEXACT_RESULT        = WINNT.STATUS_FLOAT_INEXACT_RESULT
    EXCEPTION_FLT_INVALID_OPERATION     = WINNT.STATUS_FLOAT_INVALID_OPERATION
    EXCEPTION_FLT_OVERFLOW              = WINNT.STATUS_FLOAT_OVERFLOW
    EXCEPTION_FLT_STACK_CHECK           = WINNT.STATUS_FLOAT_STACK_CHECK
    EXCEPTION_FLT_UNDERFLOW             = WINNT.STATUS_FLOAT_UNDERFLOW
    EXCEPTION_INT_DIVIDE_BY_ZERO        = WINNT.STATUS_INTEGER_DIVIDE_BY_ZERO
    EXCEPTION_INT_OVERFLOW              = WINNT.STATUS_INTEGER_OVERFLOW
    EXCEPTION_PRIV_INSTRUCTION          = WINNT.STATUS_PRIVILEGED_INSTRUCTION
    EXCEPTION_STACK_OVERFLOW            = WINNT.STATUS_STACK_OVERFLOW

    def __get__(self, instance, owner):
        return self.value


class Win32Exception(RuntimeError):
    """
    Exception class for Win32 runtime errors.

    Refs: https://docs.microsoft.com/en-us/dotnet/api/system.componentmodel.win32exception?view=netframework-4.8
    """

    def __init__(self, error = None, message = None):
        self.error = error or GetLastError()
        self.msg = message or FormatMessage(self.error)

    def __str__(self):
        return '%s (0x%08x)' % (self.msg, self.error)

    def __repr__(self):
        return 'Win32Exception(%s)' % str(self)
